/*
 * Default configuration file
 */

module.exports = {
  API_VERSION: process.env.API_VERSION || '/v1',
  LOG_LEVEL: process.env.LOG_LEVEL || 'debug',
  WEB_SERVER_PORT: process.env.PORT || 8080,
  DB_URL: process.env.DB_URL || 'mysql://root:password@database:3306/sample'
}
