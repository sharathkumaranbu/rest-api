/**
 * The application entry point
 */

require('./src/bootstrap')
const config = require('config')
const express = require('express')
const cors = require('cors')
const bodyParser = require('body-parser')
const _ = require('lodash')
const httpStatus = require('http-status-codes')
const logger = require('./src/common/logger')
const helper = require('./src/common/helper')
const errorMiddleware = require('./src/common/ErrorMiddleware')
const routes = require('./src/routes')

const app = express()

app.set('port', config.WEB_SERVER_PORT)

app.use(bodyParser.json())
app.use(bodyParser.urlencoded({ extended: true }))
app.use(cors())

const apiRouter = express.Router()

_.each(routes, (verbs, url) => {
  _.each(verbs, (def, verb) => {
    let actions = [
      (req, res, next) => {
        req.signature = `${def.controller}#${def.method}`
        next()
      }
    ]
    const method = require(`./src/controllers/${def.controller}`)[ def.method ]; // eslint-disable-line

    if (!method) {
      throw new Error(`${def.method} is undefined, for controller ${def.controller}`)
    }
    if (def.middleware && def.middleware.length > 0) {
      actions = actions.concat(def.middleware)
    }

    actions.push(method)
    logger.info(`API : ${verb.toLocaleUpperCase()} ${url}`)
    apiRouter[verb](`${url}`, helper.autoWrapExpress(actions))
  })
})

app.use('/', apiRouter)
app.use(errorMiddleware())

// Check if the route is not found or HTTP method is not supported
app.use('*', (req, res) => {
  const pathKey = req.baseUrl
  const route = routes[pathKey]
  if (route) {
    res.status(httpStatus.METHOD_NOT_ALLOWED).json({ message: 'The requested HTTP method is not supported.' })
  } else {
    res.status(httpStatus.NOT_FOUND).json({ message: 'The requested resource cannot be found.' })
  }
})

app.listen(app.get('port'), () => {
  logger.debug(`Express server listening on port ${app.get('port')}`)
})

module.exports = app
