/**
 * Sample Service
 */

const Joi = require('joi')
const models = require('../models')
const { Sample, Statistics } = require('../models')

/**
 * Create new sample
 * @param {Object} body
 */
const createSample = async (body) => {
  await models.sequelize.transaction(async (t) => {
    const statistics = await Statistics.create({ testValue: 25.0 }, { t })
    await Sample.create({ info: body.info, statisticsId: statistics.id }, { t })
  })
  return { success: true }
}

createSample.schema = {
  body: Joi.object().keys({
    info: Joi.string().required()
  })
}

/**
 * Search samples
 */
const searchSamples = async (req, res) => {
  return Sample.findAll({})
}

/**
 * Get a single sample
 */
const getSample = async (req, res) => {
  return { success: true }
}

/**
 * Update existing sample
 */
const updateSample = async (req, res) => {
  return { success: true }
}

/**
 * Delete a sample
 */
const deleteSample = async (req, res) => {
  return { success: true }
}

module.exports = {
  createSample,
  searchSamples,
  getSample,
  updateSample,
  deleteSample
}
