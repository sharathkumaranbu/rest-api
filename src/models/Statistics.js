/**
 * Schema for Statistics
 */
module.exports = (sequelize, DataTypes) =>
  sequelize.define('Statistics',
    {
      id: { type: DataTypes.BIGINT, allowNull: false, primaryKey: true, autoIncrement: true },
      testValue: { type: DataTypes.FLOAT, allowNull: false }
    },
    {
      timestamps: false
    }
  )
