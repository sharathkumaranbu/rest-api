/**
 * Schema for Sample
 */
module.exports = (sequelize, DataTypes) =>
  sequelize.define('Sample',
    {
      id: { type: DataTypes.BIGINT, allowNull: false, primaryKey: true, autoIncrement: true },
      info: { type: DataTypes.STRING }
    },
    {
      timestamps: false
    }
  )
