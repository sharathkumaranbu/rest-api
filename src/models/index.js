/**
 * Initialize and export all model schemas
 */
const config = require('config')
const Sequelize = require('sequelize')

const sequelize = new Sequelize(config.DB_URL, {
  logging: false,
  operatorsAliases: false,
  dialect: 'mysql'
})
const models = { sequelize }

// Definitions
models.Sample = sequelize.import('./Sample')
models.Statistics = sequelize.import('./Statistics')

// Associations
models.Sample.belongsTo(models.Statistics, { as: 'statistics', foreignKey: 'statisticsId' })

module.exports = models
