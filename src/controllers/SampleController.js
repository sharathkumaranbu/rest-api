/**
 * Sample Controller
 */

const SampleService = require('../services/SampleService')

/**
 * Create new sample
 * @param {Object} req
 * @param {Object} res
 */
const createSample = async (req, res) => {
  res.json(await SampleService.createSample(req.body))
}

/**
 * Search samples
 * @param {Object} req
 * @param {Object} res
 */
const searchSamples = async (req, res) => {
  res.json(await SampleService.searchSamples(req.params))
}

/**
 * Get a single sample
 * @param {Object} req
 * @param {Object} res
 */
const getSample = async (req, res) => {
  res.json(await SampleService.getSample(req.body))
}

/**
 * Update existing sample
 * @param {Object} req
 * @param {Object} res
 */
const updateSample = async (req, res) => {
  res.json(await SampleService.updateSample(req.params.id, req.body))
}

/**
 * Delete a sample
 * @param {Object} req
 * @param {Object} res
 */
const deleteSample = async (req, res) => {
  res.json(await SampleService.deleteSample(req.params.id))
}

module.exports = {
  createSample,
  searchSamples,
  getSample,
  updateSample,
  deleteSample
}
