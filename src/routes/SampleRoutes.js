/**
 * Sample API Routes
 */

module.exports = {
  '/samples': {
    post: {
      controller: 'SampleController',
      method: 'createSample'
    },
    get: {
      controller: 'SampleController',
      method: 'searchSamples'
    }
  },
  '/samples/:id': {
    get: {
      controller: 'SampleController',
      method: 'getSample'
    },
    put: {
      controller: 'SampleController',
      method: 'updateSample'
    },
    delete: {
      controller: 'SampleController',
      method: 'deleteSample'
    }
  }
}
