/**
 * Defines the API routes
 */

const _ = require('lodash')
const SampleRoutes = require('./SampleRoutes')

module.exports = _.extend({},
  SampleRoutes)
