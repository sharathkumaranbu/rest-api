FROM topcoder/topcoder-tco2018-base

# Copy the source code into working directory
COPY . /rest-api

WORKDIR /rest-api

RUN chmod +x wait-for-it.sh

# Install API dependencies
RUN npm install

# Starting point for API
CMD [ "npm", "start" ]
